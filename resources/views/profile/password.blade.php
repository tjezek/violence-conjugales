@extends('layouts.app')

@section('content')
<main>
    <div class="back">
        <div class="col col-sm-8 col-md-7 col-lg-6 col-xl-5">
            <div class="title_info"><i class="fas fa-user-circle"></i> Change password</div>
            <profile-password></profile-password>
        </div>
    </div>
</main>
@endsection