@extends('layouts.app')

@section('content')
<main>
    <div class="back">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    <div class="title_info"><i class="fas fa-book"></i> Documentation</div>
                    <p>Les navigateurs internet gardent en mémoire les sites visités et les recherches effectuées. Si
                        vous craignez que quelqu’un découvre que vous avez visité ce site, il est possible d’effacer ces
                        traces.</p>
                    <h6>EFFACER L’HISTORIQUE</h6>

                    <p>Pour effacer toute trace des sites que vous avez visités, choisissez une des marches à suivre
                        présentées ci-dessous selon le navigateur dont vous disposez.</p>

                    <h5><i class="fab fa-internet-explorer"></i> Microsoft Internet Explorer</h5>

                    <ul>
                        <li>Ouvrez le menu « Outils » en haut à droite</li>
                        <li>Choisissez « Sécurité »</li>
                        <li>Cliquez sur « Supprimer l’historique de navigation »</li>
                        <li>Cochez « Historique » et « Cookies et données de sites web »</li>
                        <li>Cliquez sur « Supprimer »</li>
                        <li>Fermer la boîte de dialogue</li>
                        <li> Vous pouvez également utiliser le clavier en combinant les touches Ctrl + Maj + Suppr pour
                            ouvrir la boîte de dialogue « Supprimer l’historique de navigation »</li>
                    </ul>

                    <h5><i class="fab fa-firefox"></i> Mozilla Firefox</h5>

                    <ul>
                        <li>Ouvrez le menu « Outils » en haut à droite</li>
                        <li>Choisissez « Options » > « Vie privée »> « Historique »</li>
                        <li>Cliquez sur « Effacer votre historique récent »</li>
                        <li>Cochez « Historique de navigation et des téléchargements » et « Cookies »</li>
                        <li>Choisissez la période à supprimer</li>
                        <li>Cliquez sur « Effacer maintenant »</li>
                        <li>Fermez la boîte de dialogue</li>
                        <li>Vous pouvez également utiliser le clavier en combinant les touches Ctrl + Maj + Suppr pour
                            ouvrir la boîte de dialogue « Effacer votre historique récent »</li>
                    </ul>

                    <h5><i class="fab fa-safari"></i> Safari</h5>

                    <ul>
                        <li>Cliquez sur le menu « Safari » en haut à gauche</li>
                        <li>Cliquez sur « Effacer l’historique »</li>
                        <li>Choisissez la période à supprimer</li>
                        <li>Cliquez sur « Effacer l’historique »</li>
                    </ul>

                    <h5><i class="fab fa-chrome"></i> Chrome</h5>

                    <ul>

                        <li>Ouvrez le menu « Outils » en haut à droite</li>
                        <li>Choisissez « Historique »</li>
                        <li>Cliquez sur « Historique »</li>
                        <li>Cliquez sur « Effacer les données de navigation »</li>
                        <li>Dans l’onglet « Général », cochez « Historique de navigation » et « Cookies et autres
                            données de site »</li>
                        <li>Cliquez sur « Effacer les données »</li>
                        <li>Vous pouvez également utiliser le clavier en combinant les touches Ctrl + Maj + Suppr pour
                            ouvrir la boîte de dialogue « Effacer les données de navigation »</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    </div>
</main>
@endsection