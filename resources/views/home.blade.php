@extends('layouts.app')

@section('content')

@if (session('status'))
<div class="alert alert-success" role="alert">
    {{ session('status') }}
</div>
@endif
<div class="banner">
<div class="cover">
    <div class="container">
        <div class="header">
                <button data-toggle="collapse" data-target="#coll1" aria-expanded="false" aria-controls="coll1">Je subis
                    de la
                    violence</button>
                <button data-toggle="collapse" data-target="#coll2" aria-expanded="false" aria-controls="coll2">Je suis
                    témoin
                    de violence</button>
        </div>
    </div>
    </div>
</div>

<main>
    <div class="collapse" id="coll1">
        <div class="row info_box">
            <div class="col col-md-6">
                <h4>Les violences conjugales c’est quoi ?</h4>
                <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Laborum molestiae provident incidunt
                    numquam
                    autem laboriosam vitae veniam architecto, dignissimos, omnis facere accusantium esse, doloribus
                    asperiores
                    deserunt. Dignissimos fugiat libero optio?</p>
                <h4>Les conséquences</h4>
                <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quos molestiae sint inventore alias dolorum
                    est
                    quas. Eius vitae sint adipisci. Voluptate perspiciatis exercitationem, earum sequi ex non sunt
                    corrupti
                    accusamus!</p>
                <h4>A qui m’adresser ?</h4>
                <ul>
                    <li>Lorem</li>
                    <li>Lorem</li>
                    <li>Lorem</li>
                </ul>
            </div>
            <div class="col col-md-6">
                <h4>Ce que dit la loi</h4>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Amet nesciunt labore quisquam tempore, et
                    quod? Et
                    aliquid hic quo doloremque, cupiditate similique eligendi. Temporibus ipsam voluptatem perspiciatis
                    accusamus rem. Error.</p>
                <h4>Des conseils pour ma sécurité.</h4>
                <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Eaque consequuntur optio accusamus. Sunt
                    aperiam
                    eveniet reprehenderit minus qui fuga! Voluptatem culpa fuga amet vitae! Hic tenetur tempore atque
                    dolorem
                    perspiciatis.</p>
            </div>
        </div>
    </div>
    </div>
    <div class="collapse" id="coll2">
        <div class="info_box">
            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim
            keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.
        </div>
    </div>

    <div class="dab">
        <h3>J’ai recours à la violence</h3>
        <hr />
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur fringilla dapibus velit eu accumsan. Nullam
        odio augue, maximus sit amet nulla sed, vestibulum consectetur nulla. Sed vel est magna. Donec luctus nibh nunc,
        a molestie leo aliquet in. Aenean vulputate quam eget augue porttitor convallis. Maecenas vel suscipit turpis,
        sed volutpat est. Sed commodo libero ac imperdiet maximus. Nam quis pretium sapien, efficitur ullamcorper metus.
        Interdum et malesuada fames ac ante ipsum primis in faucibus. Sed eu efficitur metus, quis facilisis ex. In
        blandit id augue in mattis. Nunc auctor est congue orci ornare pulvinar.
    </div>

    <div class="container">
        <div class="row justify-content-center my-5">
            <div class="col-12 col-md-5 my-1">
                <div id="news" class="title_info">L’acualité dans le pays de condé</div>
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Natus, rerum nesciunt beatae quisquam esse quia
                eligendi illum? Voluptatem itaque laboriosam exercitationem magni illo, unde consequatur officiis iusto,
                recusandae at laudantium?
            </div>
            <div class="col-12 col-md-5 my-1">
                <div id="links" class="title_info">Les liens utiles</div>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odio, recusandae, error necessitatibus aperiam
                ad consequuntur nostrum debitis vero velit eos blanditiis itaque odit magni vel mollitia laborum!
                Blanditiis, itaque consectetur.
                <ul class="my-2">
                    <li>My Link 1</li>
                    <li>My Link 2</li>
                </ul>
            </div>
        </div>
    </div>
</main>


<div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <p>En navigant sur ce site vous avez la possibilité de le quitter rapidement en cliquant sur "Sortie rapide" en haut à droite de votre fenêtre, cela vous redirigera vers google.fr</p>
                <p>Si vous souhaiter effacer les traces de votre passage, veuillez cliquer <a style="color: var(--color-secondary)" href="{{ route('doc') }}">ici</a></p>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Fermer</button>
                </div>
            </div>
        </div>
</div>
@endsection