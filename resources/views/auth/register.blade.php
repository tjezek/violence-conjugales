@extends('layouts.app')

@section('content')
<main>
    <div class="back">
        <div class="col col-sm-8 col-md-6 col-lg-5 col-xl-4">
            <div class="title_info"><i class="fas fa-user-circle"></i> Register</div>
            @if (session('confirmation-success'))
            <div class="alert alert-success">
                {{ session('confirmation-success') }}
            </div>
            @else
            <form role="form" method="POST" action="{{ url('/register') }}">
                {{ csrf_field() }}
                <div class="form-row">
                    <div class="form-group col-md-6{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label for="name">Pseudo</label>
                        <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required
                            autofocus>

                        @if ($errors->has('name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group col-md-6{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email">E-Mail</label>
                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}"
                            required>

                        @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6{{ $errors->has('password') ? ' has-error' : '' }}">
                        <label for="password">Mot de passe</label>
                        <input id="password" type="password" class="form-control" name="password" required>

                        @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group col-md-6">
                        <label for="password-confirm">Confirmer mot de passe</label>
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation"
                            required>
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-check">
                        <input type="checkbox" class="form-check-input" id="terms" required>
                        <label class="form-check-label" for="terms">Accepter les <a href="">conditions générales
                                d'utilisation</a></label>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col">
                        <button type="submit" class="btn btn-primary">Inscription</button>
                    </div>
                </div>
        </form>
        @endif
    </div>

    </div>
</main>
@endsection