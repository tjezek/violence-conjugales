<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Violfem') }}</title>

    <script src="{{ asset('js/app.js') }}" defer></script>
    <link
        href="https://fonts.googleapis.com/css?family=Raleway:200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&display=swap"
        rel="stylesheet">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('css/custom.css') }}">

    <script src="https://kit.fontawesome.com/3272b051c3.js"></script>
</head>

<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-dark fixed-top" id="banner">
            <div class="container">

                <a class="navbar-brand" href="#"><i class="fas fa-phone-alt"></i> Appelez le <span>3919</span></a>

                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="collapsibleNavbar">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('home') }}#news">L’actualité du <br/>pays de condé</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('home') }}#links">Les liens <br/>utiles</a>
                        </li>
                        @guest
                        <li class="nav-item">
                            <a class="nav-link no-border" href="{{ route('login') }}"><br/><i class="fas fa-user"></i> Compte</a>
                        </li>
                        @else
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle no-border" id="navbarDropdown" role="button" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                                <br/>{{ Auth::user()->name }}
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('profile') }}">Profile</a>
                                <a class="dropdown-item" href="{{ route('message') }}">Messagerie</a>
                                @if(Auth::user() && Auth::user()->hasRole('admin'))
                                <a class="dropdown-item" href="{{ route('adminHome') }}">Administration</a>
                                @endif
                                <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    {{ __('Déconnexion') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                    style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                        @endguest
                        <li class="nav-item">
                            <a class="nav-link no-border" href="https://www.google.fr/"><br/><i class="fas fa-sign-out-alt"></i> Sortie rapide</a>
                        </li>
                    </ul>
                </div>

            </div>
        </nav>

        @yield('content')

        <footer>
            Lorem ipsum dolor, sit amet consectetur adipisicing elit.
        </footer>

    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script>
        $(window).on("load", function () {
            $('#myModal').modal('show');
        });
    </script>
    <script>
        $(document).on("scroll", function () {
            if ($(document).scrollTop() > 86) {
                $("#banner").addClass("shrink");
            } else {
                $("#banner").removeClass("shrink");
            }
        });
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js" charset="utf-8"></script>
</body>

</html>