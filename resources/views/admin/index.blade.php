@extends('layouts.app')
<style>
    .div-table {
        display: table;
        width: 100%;
        border-spacing: 5px;
    }

    .div-table-row {
        display: table-row;
        width: auto;
        clear: both;
    }

    .div-table-col {
        float: left;
        display: table-column;
    }
</style>
@section('content')
<main>
<div class="back">
        <div class="container">

            <div class="row">
                <div class="col-12 col-sm-12 col-md-9 col-lg-9 col-xl-9">
                    <div class="title_info"><i class="fas fa-user-circle"></i> Role members</div>
                    <div class="div-table">
                        <div class="div-table-row">
                            <div class="div-table-col" style="width: 20%;">Nom</div>
                            <div class="div-table-col" style="width: 30%;">Email</div>
                            <div class="div-table-col" style="width: 15%;">Utilisateur</div>
                            <div class="div-table-col" style="width: 15%;">Administrateur</div>
                            <div class="div-table-col" style="width: 20%;"></div>
                        </div>
                        @foreach($users as $user)
                        <div class="div-table-row">
                            <form method="POST" action="{{ route('adminPostRoles') }}">
                                {{ csrf_field() }}
                                <div class="div-table-col" style="width: 20%;">{{$user->name}}</div>
                                <div class="div-table-col" style="width: 30%;"><small>{{$user->email}}</small><input
                                        type="hidden" name="email" value="{{ $user->email }}"></div>
                                <div class="div-table-col" style="width: 15%;"><input type="checkbox"
                                        {{ $user->hasRole('user') ? 'checked' : '' }} name="role_user"></div>
                                <div class="div-table-col" style="width: 15%;"><input type="checkbox"
                                        {{ $user->hasRole('admin') ? 'checked' : '' }} name="role_admin"></div>
                                <div class="div-table-col" style="width: 20%;"><button type="submit"
                                        class="btn btn-primary">Assigner</button></div>
                            </form>
                        </div>
                        @endforeach
                    </div>
                </div>

                <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
                    <div class="title_info"><i class="fas fa-info-circle"></i> Some info</div>
                </div>
            </div>

            <div class="row">
                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6" style="height: 300px;">
                    <div class="title_info" style="margin: 0;"><i class="fas fa-chart-pie"></i> Charts</div>

                    {!! $user_chart->container() !!}

                </div>
            </div>

        </div>
        
    </div>
</main>
{!! $user_chart->script() !!}
@endsection