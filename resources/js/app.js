require('./bootstrap');


window.Vue = require('vue');

import Toasted from 'vue-toasted';
import Vue from 'vue';

Vue.use(Toasted)

Vue.toasted.register('error', message => message, {
    theme: "outline", 
    position : 'bottom-center',
    duration : 2500
})

Vue.component('profile', require('./components/profile/Profile.vue').default);
Vue.component('profile-password', require('./components/profile/Password.vue').default);
Vue.component('profile-message', require('./components/profile/Message.vue').default);

const app = new Vue({
    el: '#app',
});