<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');
Route::get('/doc', function () {
    return view('doc');
})->name('doc');

Auth::routes();

Route::middleware('auth')->group(function () {
    Route::get('/admin', 'AdminController@index')->name('adminHome');
    Route::post('/admin', 'AdminController@postAdminRoles')->name('adminPostRoles');

});

require __DIR__ . '/profile/profile.php';
