<?php

Route::middleware('auth')->group(function () {
    Route::group(['namespace' => 'Profile'], function () {

        Route::view('/profile', 'profile.profile')->name('profile');
        Route::view('/password', 'profile.password')->name('updatePassword');
        Route::view('/message', 'profile.message')->name('message');

        Route::group(['prefix' => 'api/profile'], function () {
            Route::get('/getAuthUser', 'ProfileController@getAuthUser');
            Route::put('/updateAuthUser', 'ProfileController@updateAuthUser');
            Route::put('/updateAuthUserPassword', 'ProfileController@updateAuthUserPassword');  
            Route::delete('/destroyAuthUser', 'ProfileController@destroyAuthUser');

            Route::get('/getUsersAll', 'ProfileController@getUsersAll');

            Route::post('/sendMessage', 'ProfileController@sendMessage');
            Route::get('/getSentMessages', 'ProfileController@getSentMessages');
            Route::get('/getRecMessages', 'ProfileController@getRecMessages');
            Route::delete('/destroyMessage/{id}', 'ProfileController@destroyMessage');
        });
    });
});
