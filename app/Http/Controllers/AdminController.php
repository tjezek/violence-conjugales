<?php

namespace App\Http\Controllers;

use App\Role;
use App\User;
use App\Message;
use App\Charts\AdminChart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class AdminController extends Controller
{
    public function index(Request $request)
    {
        $request->user()->authorizeRoles('admin');
        
        $data = collect([]);

        for ($days_backwards = 2; $days_backwards >= 0; $days_backwards--) {
            $data->push(User::whereDate('created_at', today()->subDays($days_backwards))->count());
        }

        Log::info($data);

        $user_chart = new AdminChart;
        $user_chart->labels(['il y a 2 jours', 'Hier', 'Aurjoud\'hui']);
        $user_chart->dataset('Les derniers inscrits', 'bar', $data);

        $users = User::all();
        return view('admin/index', ['users' => $users, 'user_chart' => $user_chart]);
    }

    public function postAdminRoles(Request $request)
    {
        $user = User::where('email', $request['email'])->firstOrFail();
        $user->roles()->detach();
        Log::info($user);
        if ($request['role_user']) {
            $user->roles()->attach(Role::where('name', 'user')->first());
        }
        if ($request['role_admin']) {
            $user->roles()->attach(Role::where('name', 'admin')->first());
        }
        return redirect()->back();
    }
}
