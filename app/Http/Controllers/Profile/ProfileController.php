<?php

namespace App\Http\Controllers\Profile;

use App\User;
use App\Message;
use App\File;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    public function getAuthUser()
    {
        return Auth::user();
    }

    public function updateAuthUser(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string',
            'email' => 'required|email|unique:users,email,'.Auth::id(),
        ]);

        $user = User::find(Auth::id());
        $user->name = $request->name;
        $user->email = $request->email;
        $user->save();

        return $user;
    }

    public function updateAuthUserPassword(Request $request)
    {
        $this->validate($request, [
            'current' => 'required',
            'password' => 'required|confirmed',
            'password_confirmation' => 'required'
        ]);

        $user = User::find(Auth::id());

        if (!Hash::check($request->current, $user->password)) {
            return response()->json(['errors' => ['current'=> ['Le mot de passe actuel ne correspond pas']]], 422);
        }

        $user->password = Hash::make($request->password);
        $user->save();

        return $user;
    }

    public function destroyAuthUser()
    {
        $user = User::find(Auth::id());
        $user->delete();
        return $user;
    }

    public function sendMessage(Request $request)
    {

        $this->validate($request, [
            'body' => 'required|string',
            'subject' => 'string',
            'sent_to_id' => '',
        ]);

        if($request->get('image'))
           {
              $image = $request->get('image');
              $name = time().'.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
              \Image::make($request->get('image'))->save(public_path('images/stock/').$name);
        }

        Log::info($request);

        $message = Auth::user()->sent()->create([
            'body'       => $request->body,
            'subject'    => $request->subject ?: 'test',
            'sent_to_id' => $request->sent_to_id ?: 1,
            'image'      => $request->image = $name,
        ]);

        Log::info($message);
        
        return $message;
    }

    public function getSentMessages()
    {
        $messages = Auth::user()->sent;
        return $messages;
    }

    public function getRecMessages()
    {
        $messages = Auth::user()->received;
        return $messages;
    }

    public function getUsersAll()
    {
        $users = User::all();
        Log::info($users);

        return $users;
    }

    public function destroyMessage($id)
    {
        Message::destroy($id);
    }
}