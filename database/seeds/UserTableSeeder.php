<?php

use App\Role;
use App\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    public function run()
    {
        $role_employee = Role::where('name', 'utilisateur')->first();
        $role_manager = Role::where('name', 'administrateur')->first();
        $employee = new User();
        $employee->name = 'user';
        $employee->email = 'user@user.fr';
        $employee->password = bcrypt('secret');
        $employee->save();
        $employee->roles()->attach($role_employee);
        $manager = new User();
        $manager->name = 'admin';
        $manager->email = 'admin@admin.fr';
        $manager->password = bcrypt('secret');
        $manager->save();
        $manager->roles()->attach($role_manager);
    }
}
